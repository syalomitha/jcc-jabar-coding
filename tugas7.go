package main

import "fmt"

type fruit struct {
	name, color, biji string
	price             int
}

func main() {
	fmt.Println()
	var buah1 = fruit{"Nanas", "Kuning", "Tidak Berbiji", 9000}
	var buah2 = fruit{"Jeruk", "Orange", "Ada Biji", 8000}
	var buah3 = fruit{"Semangka", "Hijau Merah", "Ada Biji", 10000}
	var buah4 = fruit{"Pisang", "Kuning", "Tidak Berbiji", 5000}

	//soal1
	fmt.Println(buah1)
	fmt.Println(buah2)
	fmt.Println(buah3)
	fmt.Println(buah4)

}
